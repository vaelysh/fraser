---
title: Auto-thérapie
draft: false
---

Les articles disponibles sur ce site visent à encourager les personnes qui bégaient à travailler pour améliorer leur élocution et à leur donner une nouvelle manière de comprendre leur difficultés et une approche pour les surmonter.

La première étape est d’accepter la nécessité du changement et d'être prêt à s'engager pleinement: vous devez vraiment vouloir faire quelque chose pour améliorer votre parole. Il n'existe pas de solution magique pour le bégaiement et vos difficultés ne disparaîtront pas toutes seules.

Bien entendu un tel changement est difficile car les habitudes en place peuvent être difficiles à modifier. Vous êtes peut-être plus ou moins à l'aise avec votre manière actuelle de parler malgré vos difficultés.

Cependant pour atteindre des résultats durables, il faut se débarrasser des vieilles solutions qui n'ont pas fonctionné et accepter l'inconfort et la douleur temporaire du changement. La seule manière d'y arriver est de décomposer la problématique du bégaiement et de résoudre chaque composante individuellement.